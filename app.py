listMain = ["Harry Houdini", "Newton", "David Blaine", "Hawking", "Messi", "Teller", "Einstein", "Pele", "Juanes"]

magicians = ["Harry Houdini", "David Blaine", "Teller"]

scientists = ["Newton", "Hawking", "Einstein"]

notOthers = magicians + scientists

others = [i for i in listMain if i not in notOthers]

def hacer_grandioso(list, prefix):
    modList = []
    for i in list:
        modList.append(prefix + i)
    list[:] = modList[:]

hacer_grandioso(magicians, "El gran ")

# podriamos hacer lo mismo de la funcion en una linea con un list comprehension
# magicians = ["El gran "+i for i in magicians]

def imprimir_nombres(*lists):
    for list in lists:
        if list == listMain:
            print("Lista original:\n", *list, "\n", sep="\n")
        elif list == magicians:
            print("Lista de (grandes) magos:\n", *list, "\n", sep="\n")
        elif list == scientists:
            print("Lista de científicos:\n", *list, "\n", sep="\n")
        elif list == others:
            print("Lista de otros:\n", *list, "\n", sep="\n")

imprimir_nombres(listMain, magicians, scientists, others)